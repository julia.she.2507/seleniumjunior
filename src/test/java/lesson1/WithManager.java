package lesson1;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WithManager {
    private WebDriver driver;
    @Before
    public void before(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }
    @After
    public void after(){
        if (driver != null)
        driver.quit();
    }
    @Test
    public void test() {
        driver.get("https://yandex.ru/"); //загрузить страницу
        Assert.assertEquals("Яндекс", driver.getTitle());


    }
    @Test
    public void test2(){
        //
    }
}
