package lesson1;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

public class WithOutManager {
    ChromeDriver driver;
    //предварительно нужно скачать драйвер последней версии
    @Before
    public void before() {
        System.setProperty("WebDriver.chrome.driver()", "C:\\webdriver\\chromedriver\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @After
    public void after() {
        if (driver != null)
            driver.quit();
    }

    @Test
    public void test() {
        driver.get("https://yandex.ru/"); //загрузить страницу
        Assert.assertEquals("Яндекс", driver.getTitle());

    }
}
